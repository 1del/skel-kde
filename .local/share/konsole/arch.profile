[Appearance]
ColorScheme=dark_default

[Cursor Options]
CursorShape=1

[General]
Name=arch
Parent=FALLBACK/

[Terminal Features]
BlinkingCursorEnabled=true
